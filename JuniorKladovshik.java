class JuniorKladovshik extends Kladovshik {
    public void sort() {
        long startTime = System.nanoTime();
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Пузырьковая сортировка завершена за  " + duration + " наносекунд.");
    }
}