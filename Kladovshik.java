import java.util.Arrays;

abstract class Kladovshik {
    protected int[] numbers;

    public Kladovshik() {
        numbers = new int[10];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) (Math.random() * 10) + 1;
        }
    }

    public abstract void sort();
}
