import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        Kladovshik junior = new JuniorKladovshik();
        junior.sort();
        System.out.println(Arrays.toString(junior.numbers));

        Kladovshik senior = new SeniorKladovshik();
        senior.sort();
        System.out.println(Arrays.toString(senior.numbers));
    }
}